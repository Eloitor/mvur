# Minimal VUR helper

Minimal helper for Void User Repositories.

## Installation

### Install dependencies
```
sudo xbps-install git
```

### Bootstrap mvur
```
git clone https://codeberg.org/eloitor/mvur
cd mvur
./mvur --update
./mvur mvur-git
```

## Update templates
```
mvur --update
```

## To build and install a template (from VUR or from void-packages)
```
mvur ytfzf
```

## To edit a template
```
mvur --edit ytfzf
```

## You can access the git repository of your VUR to commit and push your changes:
```
mvur --git add ytfzf
mvur --git commit -m "Added ytfzf template"
mvur --git push
```

## You can access the xbps-src tool
```
mvur --xbps-src clean
```

## You can add pending PR to your VUR easly
```
mvur -c git checkout origin pull/36504/head:threejs-sage
mvur -c git checkout threejs-sage
mvur -g add threejs-sage
mvur -g commit -m "Added threejs-sage"
mvur -g push
```

## Usage info
```
mvur --help
```

To update `mvur-git` you have to manually remove the old one before runing `./mvur mvur-git`
